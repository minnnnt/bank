package main

import (
	"fmt"
	"sync"
)

type Bank struct {
	balance int
}

func (b *Bank) Deposit(amount int) {
	b.balance += amount
}

func (b *Bank) Balance() int {
	return b.balance
}

// go run -race main.go
// WARNING: DATA RACE
// 並行執行3次，雖然總金額正確，但是 發生 race condition
// 如果執行次數增加到1000次，就可以看出金額加總錯誤
//func main() {
//	var wg sync.WaitGroup
//	b := &Bank{}
//
//	wg.Add(3)
//	go func() {
//		b.Deposit(1000)
//		wg.Done()
//	}()
//	go func() {
//		b.Deposit(1000)
//		wg.Done()
//	}()
//	go func() {
//		b.Deposit(1000)
//		wg.Done()
//	}()
//
//	wg.Wait()
//
//	fmt.Println(b.Balance())
//}

func main() {
	var wg sync.WaitGroup
	b := &Bank{}

	n := 1000
	wg.Add(n)
	for i := 1; i <= n; i++ {
		go func() {
			b.Deposit(1000)
			wg.Done()
		}()
	}

	fmt.Println(b.Balance())
}
