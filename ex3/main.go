package main

import (
	"fmt"
	"sync"
)

type Bank struct {
	balance int
	mux     sync.Mutex
}

func (b *Bank) Deposit(amount int) {
	b.mux.Lock()
	b.balance += amount
	b.mux.Unlock()
}

func (b *Bank) Balance() int {
	return b.balance
}

// go run -race main.go
// WARNING: DATA RACE
// 寫有鎖，讀沒有所，所以還是會發生 race condition
// 但是金額加總 = 1000000
func main() {
	var wg sync.WaitGroup

	b := &Bank{}

	n := 1000
	wg.Add(n)
	for i := 1; i <= n; i++ {
		go func() {
			b.Deposit(1000)
			wg.Done()
		}()
	}
	wg.Add(n)
	for i := 1; i <= n; i++ {
		go func() {
			_ = b.Balance()
			wg.Done()
		}()
	}

	wg.Wait()

	fmt.Println(b.Balance())
}
