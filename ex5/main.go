package main

import (
	"log"
	"sync"
	"time"
)

type Bank struct {
	balance int
	mux     sync.RWMutex
}

func (b *Bank) Deposit(amount int) {
	b.mux.Lock()
	time.Sleep(time.Second) // spend 1 second
	b.balance += amount
	b.mux.Unlock()
}

func (b *Bank) Balance() (balance int) {
	b.mux.RLock()
	time.Sleep(time.Second) // spend 1 second
	balance = b.balance
	b.mux.RUnlock()
	return
}

// go run -race main.go
// No DATA RACE
// 讀加上讀鎖，寫加上寫鎖，沒有發生 race condition
// 寫堵塞，讀沒有堵塞
func main() {
	var wg sync.WaitGroup
	b := &Bank{}

	n := 5
	wg.Add(n)
	for i := 1; i <= n; i++ {
		go func() {
			b.Deposit(1000)
			log.Printf("Write: deposit amonut: %v", 1000)
			wg.Done()
		}()
	}
	wg.Add(n)
	for i := 1; i <= n; i++ {
		go func() {
			log.Printf("Read: balance: %v", b.Balance())
			wg.Done()
		}()
	}

	wg.Wait()
}
