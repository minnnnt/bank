package main

import (
	"fmt"
	"sync"
)

type Bank struct {
	balance int
	mux     sync.Mutex
}

func (b *Bank) Deposit(amount int) {
	b.mux.Lock()
	b.balance += amount
	b.mux.Unlock()
}

func (b *Bank) Balance() int {
	return b.balance
}

// go run -race main.go
// NO DATA RACE
// Deposit加上互斥鎖
// 金額加總 = 1000000
func main() {
	var wg sync.WaitGroup

	b := &Bank{}

	n := 1000
	wg.Add(n)
	for i := 1; i <= n; i++ {
		go func() {
			b.Deposit(1000)
			wg.Done()
		}()
	}
	wg.Wait()

	fmt.Println(b.Balance())
}
